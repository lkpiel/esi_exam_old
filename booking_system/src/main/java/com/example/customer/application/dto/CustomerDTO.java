package com.example.customer.application.dto;

import com.example.reservation.application.dto.ReservationDTO;
import lombok.*;

import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Data
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class CustomerDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    String name;
    String address;
    String email;

    List<ReservationDTO> reservations;
}
