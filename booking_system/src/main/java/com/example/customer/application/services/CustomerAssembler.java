package com.example.customer.application.services;

import com.example.customer.application.dto.CustomerDTO;
import com.example.customer.domain.model.Customer;
import com.example.customer.rest.controller.CustomerRestController;
import com.example.reservation.application.services.ReservationAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class CustomerAssembler  extends ResourceAssemblerSupport<Customer, CustomerDTO> {
    @Autowired
    ReservationAssembler reservationAssembler;

    public CustomerAssembler() {
        super(CustomerRestController.class, CustomerDTO.class);
    }

    @Override
    public CustomerDTO toResource(Customer customer) {
        if (customer != null) {
            CustomerDTO dto = createResourceWithId(customer.getId(), customer);
            dto.set_id(customer.getId());
            dto.setName(customer.getName());
            dto.setEmail(customer.getEmail());
            dto.setAddress(customer.getAdderss());
            dto.setReservations(reservationAssembler.toResources(customer.getReservations()));
            return dto;
        }
        return null;
    }
    public Customer toEntity(CustomerDTO customerDTO) {
        if(customerDTO != null) {
            Customer customer = Customer.of(customerDTO.get_id(),customerDTO.getName(),customerDTO.getAddress(),customerDTO.getEmail(), reservationAssembler.toEntities(customerDTO.getReservations()));
            return customer;
        }
        return null;
    }
}