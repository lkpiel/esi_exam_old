package com.example.customer.rest.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by lkpiel on 6/8/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {
}
