package com.example.room.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class CustomerIdentifierFactory {
    public String nextCustomerID() {
        return UUID.randomUUID().toString();
    }
}
