package com.example.customer.domain.model;

import com.example.reservation.domain.model.Reservation;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Customer {
    @Id
    String id;
    String name;
    String adderss;
    String email;

    @OneToMany
    List<Reservation> reservations;
}
