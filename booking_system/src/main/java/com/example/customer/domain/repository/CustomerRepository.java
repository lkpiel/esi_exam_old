package com.example.customer.domain.repository;

import com.example.customer.domain.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lkpiel on 6/8/2017.
 */
public interface CustomerRepository extends JpaRepository<Customer, String> {
}
