package com.example.reservation.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Philosoraptor on 06/03/2017.
 */
@Service
public class ReservationIdentifierFactory {
    public String nextReservationID() {
        return UUID.randomUUID().toString();
    }
}