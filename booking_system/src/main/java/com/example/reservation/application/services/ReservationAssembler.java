package com.example.reservation.application.services;

import com.example.common.rest.ExtendedLink;
import com.example.customer.application.services.CustomerAssembler;
import com.example.reservation.application.dto.ReservationDTO;
import com.example.reservation.domain.model.Reservation;
import com.example.reservation.rest.controllers.ReservationRestController;
import com.example.room.application.services.RoomAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.POST;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class ReservationAssembler extends ResourceAssemblerSupport<Reservation, ReservationDTO> {

    @Autowired
    CustomerAssembler customerAssembler;
    @Autowired
    RoomAssembler roomAssembler;
    public ReservationAssembler() {
        super(ReservationRestController.class, ReservationDTO.class);
    }

    @Override
    public ReservationDTO toResource(Reservation reservation) {
        if (reservation != null) {
            ReservationDTO dto = createResourceWithId(reservation.getId(), reservation);
            dto.set_id(reservation.getId());
            dto.setCheckIn(reservation.getCheckIn());
            dto.setCheckOut(reservation.getCheckOut());
            dto.setCustomer(customerAssembler.toResource(reservation.getCustomer()));
            dto.setRoom(roomAssembler.toResource(reservation.getRoom()));


            dto.add(new ExtendedLink(
                    linkTo(methodOn(ReservationRestController.class)
                            .cancelBooking(dto.get_id())).toString(),
                    "cancel", POST));

            return dto;
        }

        return null;
    }
    public Reservation toEntity(ReservationDTO reservationDTO) {
        if(reservationDTO != null) {
            Reservation reservation = Reservation.of(reservationDTO.get_id(),reservationDTO.getCheckIn(), reservationDTO.getCheckOut(), customerAssembler.toEntity(reservationDTO.getCustomer()),roomAssembler.toEntity(reservationDTO.getRoom()));
            return reservation;
        }
        return null;
    }
    public List<ReservationDTO> toResources(List<Reservation> reservations) {
        if(reservations.size() < 1){
            return new ArrayList<ReservationDTO>();
        }
        return reservations.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }
    public List<Reservation> toEntities(List<ReservationDTO> reservationDTOS) {
        if(reservationDTOS == null){
            return new ArrayList<Reservation>();
        }
        return reservationDTOS.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }


}
