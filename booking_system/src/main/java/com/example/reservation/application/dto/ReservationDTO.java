package com.example.reservation.application.dto;

import com.example.customer.application.dto.CustomerDTO;
import com.example.room.application.dto.RoomDTO;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Data
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class ReservationDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate checkIn;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate checkOut;
    CustomerDTO customer;
    RoomDTO room;
}
