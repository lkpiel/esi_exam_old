package com.example.reservation.application.services;

import com.example.customer.application.services.CustomerAssembler;
import com.example.customer.domain.model.Customer;
import com.example.customer.domain.repository.CustomerRepository;
import com.example.reservation.application.dto.ReservationDTO;
import com.example.reservation.domain.model.Reservation;
import com.example.reservation.domain.repository.ReservationRepository;
import com.example.reservation.infrastructure.ReservationIdentifierFactory;
import com.example.room.application.dto.RoomDTO;
import com.example.room.application.services.RoomAssembler;
import com.example.room.application.services.RoomService;
import com.example.room.domain.model.Room;
import com.example.room.domain.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class ReservationService {
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    ReservationAssembler reservationAssembler;
    @Autowired
    ReservationIdentifierFactory reservationIdentifierFactory;
    @Autowired
    RoomService roomService;
    @Autowired
    CustomerAssembler customerAssembler;
    @Autowired
    RoomAssembler roomAssembler;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    CustomerRepository customerRepository;

    public ReservationDTO createBooking(ReservationDTO reservationDTO) {
        Map<List<RoomDTO>, Integer> availableRooms = roomService.findAvailableRooms(reservationDTO.getCheckIn(), reservationDTO.getCheckOut());
        Room wantedRoom = roomRepository.findOne(reservationDTO.getRoom().get_id());


         //THIS IS JUST FOR TESTING PURPOSES
        Customer customer = customerAssembler.toEntity(reservationDTO.getCustomer());
        customerRepository.save(customer);


         reservationDTO.setRoom(roomAssembler.toResource(wantedRoom));
         for(List<RoomDTO> roomsList : availableRooms.keySet()){
             for(RoomDTO room : roomsList){
                 if(room.get_id().equals(reservationDTO.getRoom().get_id())){

                     Reservation reservation = Reservation.of(reservationIdentifierFactory.nextReservationID(),reservationDTO.getCheckIn(),reservationDTO.getCheckOut(),customerAssembler.toEntity(reservationDTO.getCustomer()),roomAssembler.toEntity(reservationDTO.getRoom()));
                     reservationRepository.save(reservation);

                     return reservationAssembler.toResource(reservation);
                 }
             }
         }
         return null;
    }

    public ReservationDTO cancelReservation(String id) {
        Reservation reservation = reservationRepository.findOne(id);
        if(LocalDate.now().isBefore(reservation.getCheckIn().minusDays(10l))){
            System.out.println("TULEKS SEADA RESEVATION STATUSEKS CANCELLED");
            return reservationAssembler.toResource(reservation);
        } else {
            return null;
        }
    }
}
