package com.example.reservation.rest.controllers;

import com.example.reservation.application.dto.ReservationDTO;
import com.example.reservation.application.services.ReservationAssembler;
import com.example.reservation.application.services.ReservationService;
import com.example.reservation.domain.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

/**
 * Created by lkpiel on 5/24/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/reservations")
public class ReservationRestController {
    @Autowired
    ReservationService reservationService;
    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    ReservationAssembler reservationAssembler;

    @GetMapping("")
    public List<ReservationDTO> findAll() {
        return reservationAssembler.toResources(reservationRepository.findAll());
    }
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ReservationDTO> makeBooking(@RequestBody ReservationDTO reservationDTO) {
        System.out.println("Making booking with " + reservationDTO.toString());
        ReservationDTO createdReservationDTO = reservationService.createBooking(reservationDTO);
        if(createdReservationDTO == null){
            return new ResponseEntity<ReservationDTO>(null, null, HttpStatus.CONFLICT);

        }else {
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create((createdReservationDTO.getId().getHref())));
            return new ResponseEntity<ReservationDTO>(createdReservationDTO, headers, HttpStatus.CREATED);
        }
    }
    @PostMapping("{id}/cancel")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ReservationDTO> cancelBooking(@PathVariable String id) {
        ReservationDTO cancelledReservation = reservationService.cancelReservation(id);
        if(cancelledReservation == null){
            return new ResponseEntity<ReservationDTO>(null, null, HttpStatus.CONFLICT);

        }else {
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create((cancelledReservation.getId().getHref())));
            return new ResponseEntity<ReservationDTO>(cancelledReservation, headers, HttpStatus.CREATED);
        }
    }
}
