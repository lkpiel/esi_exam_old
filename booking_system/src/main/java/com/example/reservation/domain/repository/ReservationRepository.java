package com.example.reservation.domain.repository;

import com.example.reservation.domain.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lkpiel on 2/18/2017.
 */
public interface ReservationRepository extends JpaRepository<Reservation, String> {

}
