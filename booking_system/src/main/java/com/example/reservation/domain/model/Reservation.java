package com.example.reservation.domain.model;

import com.example.customer.domain.model.Customer;
import com.example.room.domain.model.Room;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Reservation {
    @Id
    String id;
    LocalDate checkIn;
    LocalDate checkOut;

    @ManyToOne
    Customer customer;

    @ManyToOne
    Room room;

}
