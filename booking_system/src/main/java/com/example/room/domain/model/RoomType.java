package com.example.room.domain.model;

/**
 * Created by lkpiel on 6/8/2017.
 */
public enum RoomType {
    SINGLE, DOUBLE, TWIN, STUDIO
}
