package com.example.room.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Room {
    @Id
    String id;
    @Enumerated(EnumType.STRING)
    RoomType roomType;
    @Column(precision = 8, scale = 2)
    BigDecimal price;

}
