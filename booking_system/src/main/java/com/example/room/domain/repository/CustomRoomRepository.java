package com.example.room.domain.repository;

import com.example.room.domain.model.Room;
import com.example.room.domain.model.RoomType;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 6/8/2017.
 */
public interface CustomRoomRepository {
    Map<RoomType, List<Room>> findAvailableRooms(LocalDate startData, LocalDate endDate);
}
