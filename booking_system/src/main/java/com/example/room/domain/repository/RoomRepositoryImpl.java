package com.example.room.domain.repository;

import com.example.reservation.domain.model.Reservation;
import com.example.room.domain.model.Room;
import com.example.room.domain.model.RoomType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 6/8/2017.
 */
public class RoomRepositoryImpl implements CustomRoomRepository{
    @Autowired
    EntityManager em;

    @Override
    public Map<RoomType, List<Room>> findAvailableRooms(LocalDate startDate, LocalDate endDate) {
        Map<RoomType, List<Room>> resultMap = new HashMap<>();
        if ((startDate.isEqual(LocalDate.now()) || startDate.isAfter(LocalDate.now())) && endDate.isAfter(LocalDate.now())) {
            List<Room> rooms = em.createQuery("SELECT room FROM Room room WHERE room.id NOT IN (SELECT r.room.id FROM Reservation r WHERE (?1 >= r.checkOut) AND (?2 <= r.checkIn))")
                    .setParameter(1, startDate)
                    .setParameter(2, endDate).getResultList();



            resultMap = new HashMap<>(RoomType.values().length);
            for (Room room : rooms){
                if(resultMap.containsKey(room.getRoomType())){
                    List<Room> roomsWithCertainType = resultMap.get(room.getRoomType());
                    roomsWithCertainType.add(room);
                    resultMap.put(room.getRoomType(),roomsWithCertainType);
                } else {
                    List<Room> newRooms = new ArrayList<>();
                    newRooms.add(room);
                    resultMap.put(room.getRoomType(), newRooms
                    );
                }
            }

        }

        return resultMap;
    }
}
