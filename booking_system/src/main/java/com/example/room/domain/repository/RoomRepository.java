package com.example.room.domain.repository;

import com.example.room.domain.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, String>, CustomRoomRepository{

}
