package com.example.room.application.dto;

import com.example.room.domain.model.RoomType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class RoomDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    RoomType roomType;
    BigDecimal price;
}