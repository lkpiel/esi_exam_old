package com.example.room.application.services;

import com.example.room.application.dto.RoomDTO;
import com.example.room.domain.model.Room;
import com.example.room.rest.controller.RoomRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class RoomAssembler extends ResourceAssemblerSupport<Room, RoomDTO> {

    public RoomAssembler() {
        super(RoomRestController.class, RoomDTO.class);
    }

    @Override
    public RoomDTO toResource(Room room) {
        if (room != null) {
            RoomDTO dto = createResourceWithId(room.getId(), room);
            dto.set_id(room.getId());
            dto.setRoomType(room.getRoomType());
            dto.setPrice(room.getPrice());
            return dto;
        }
        return null;
    }

    public Room toEntity(RoomDTO roomDTO) {
        if (roomDTO != null) {
            Room room = Room.of(roomDTO.get_id(), roomDTO.getRoomType(), roomDTO.getPrice());
            return room;
        }
        return null;
    }
    public List<RoomDTO> toResources(List<Room> rooms) {
        if(rooms.size() < 1){
            return new ArrayList<RoomDTO>();
        }
        return rooms.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }
}

