package com.example.room.application.services;

import com.example.common.dto.AvailableRoomDTO;
import com.example.room.application.dto.RoomDTO;
import com.example.room.domain.model.Room;
import com.example.room.domain.model.RoomType;
import com.example.room.domain.repository.RoomRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class RoomService {
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    RoomAssembler roomAssembler;
    public Map<List<RoomDTO>, Integer> findAvailableRooms(LocalDate startDate, LocalDate endDate) {
        Map<RoomType, List<Room>> available = roomRepository.findAvailableRooms(startDate, endDate);
        Gson g = new Gson();
        System.out.println(g.toJson(available));
        List<Room> rooms = new ArrayList<>();
        AvailableRoomDTO availableRoomDTOS;
        Map<List<RoomDTO>, Integer> availableRooms = new HashMap<>();
        for(RoomType type : available.keySet()){
            availableRooms.put(roomAssembler.toResources(available.get(type)), available.get(type).size());

        }
        return availableRooms;

    }
}
