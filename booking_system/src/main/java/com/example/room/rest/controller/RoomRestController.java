package com.example.room.rest.controller;

import com.example.room.application.dto.RoomDTO;
import com.example.room.application.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by lkpiel on 6/8/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/rooms")
public class RoomRestController {
    @Autowired
    RoomService roomService;
    @GetMapping("")
    public  Map<List<RoomDTO>,Integer> findAvailablePlants(
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDateURI,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDateURI) {

        if (startDateURI.isPresent() && endDateURI.isPresent()) {
            if (endDateURI.get().isBefore(startDateURI.get()))
                throw new IllegalArgumentException("Something wrong with the requested period ('endDate' happens before 'startDate')");
        } else
            throw new IllegalArgumentException(
                    String.format("Wrong number of " +
                            "parameters: Start date='%s', End date='%s'", startDateURI.get(), endDateURI.get()));

        // get plants from rentIt side
        Map<List<RoomDTO>, Integer> roomsDTOs = roomService.findAvailableRooms( startDateURI.get(), endDateURI.get());

        return roomsDTOs;
    }
}
