package com.example.common.dto;

import com.example.room.application.dto.RoomDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class AvailableRoomDTO {
    Map<List<RoomDTO>, Integer> availableRooms;
}
